package pl.mateusz_szczukowski;

/**
 * Created by Mateusz on 11.11.2016.
 */
public class Order {

    private long id;
    public clientDetails client;
    public Address deliveryAddress;
    public orderItem items;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public clientDetails getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public Address getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public orderItem getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }
}
